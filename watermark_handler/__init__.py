import cv2
import numpy as np

from watermark_handler.WaterMarkDestFileHandler import WaterMarkDestFileHandler


class WatermarkHandler:

    def __init__(self, input_bucket_name):
        self.dest_handler = WaterMarkDestFileHandler(input_bucket_name, "watermark", prefix="WATERMARK")

    def decode_watermark_info_legacy(self, watermark_info):
        watermark_splitted = dict(enumerate(watermark_info.split("/")))
        watermark_category = watermark_splitted.get(0)
        orientation = watermark_splitted.get(5)
        return watermark_category, orientation, 16

    def decode_watermark_info(self, watermark_info):
        watermark_splitted = dict(enumerate(watermark_info.split("/")))
        if len(watermark_splitted) > 3:
            watermark_category, orientation, alpha = self.decode_watermark_info_legacy(watermark_info)
        else:
            watermark_category = watermark_splitted.get(0)
            alpha = int(watermark_splitted.get(1))
            orientation = watermark_splitted.get(2)
        return watermark_category, orientation, alpha

    def apply_frames_with_watermark_id(self, frames, watermark_info):
        watermark_category, orientation, alpha = self.decode_watermark_info(watermark_info)
        return self.apply_frames(watermark_category, alpha, frames, orientation)

    def apply_frames(self, category, alpha, frames, orientation, x_padding=1, y_padding=1):
        result_frames = []

        if len(frames) == 0:
            return result_frames

        h_img, w_img, _ = frames[0].shape

        file_id, watermark_bytes, percent_of_scaling = self.dest_handler.pick_watermark(category, h_img, alpha)
        if watermark_bytes is None:
            return frames
        logo_image = cv2.imdecode(np.fromstring(watermark_bytes, np.uint8), cv2.IMREAD_COLOR)
        new_width = int(logo_image.shape[1] * percent_of_scaling / 100)
        new_height = int(logo_image.shape[0] * percent_of_scaling / 100)
        new_dim = (new_width, new_height)
        resized_img = cv2.resize(logo_image, new_dim, interpolation=cv2.INTER_AREA)
        h_logo, w_logo, c = resized_img.shape

        if orientation == "UL":
            top_y = y_padding
            bottom_y = h_logo + y_padding
            left_x = x_padding
            right_x = w_logo + x_padding
        elif orientation == "UR":
            top_y = y_padding
            bottom_y = h_logo + y_padding
            left_x = w_img - w_logo - x_padding
            right_x = w_img - x_padding
        elif orientation == "DL":
            top_y = h_img - h_logo - y_padding
            bottom_y = h_img - y_padding
            left_x = x_padding
            right_x = w_logo + x_padding
        else:
            top_y = h_img - h_logo - y_padding
            bottom_y = h_img - y_padding
            left_x = w_img - w_logo - x_padding
            right_x = w_img - x_padding

        for frame in frames:
            roi = frame[top_y:bottom_y, left_x:right_x]
            result = cv2.addWeighted(roi, 1, np.asarray(resized_img, frame.dtype), 1, 0)
            frame[top_y:bottom_y, left_x:right_x] = result
            result_frames.append(frame)
        return result_frames
