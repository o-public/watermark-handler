import cv2
import numpy as np
from s3_image_handler.dest_file_handler import DestFileHandler


class WaterMarkDestFileHandler(DestFileHandler):
    def __init__(self, bucket_name, local_dest_folder, prefix='DEST', org_suffix='ORG', aligned_suffix='ALIGNED',
                 latent_suffix='LATENT', get_url_expire=3600):
        super().__init__(bucket_name, local_dest_folder, prefix, org_suffix, aligned_suffix, latent_suffix,
                         get_url_expire)
        self.dict = self.create_dict()

    def list_remote_org_files(self, category=None):
        def filter_image(id):
            return id.startswith(self.prefix + self.remote_delimiter) and id.endswith(self.org_suffix)

        result = self.client.list_objects_v2(Bucket=self.bucket_name, Prefix=self.prefix + self.remote_delimiter)
        file_list = list(filter(filter_image, map(lambda content: content['Key'], result['Contents'])))

        return file_list

    def create_dict(self):
        remote_files = self.list_remote_org_files()
        file_dict = {}
        for file in remote_files:
            category = file.split("/")[1]
            file_id = file.split("/")[2]
            file_bytes = self.get_org_remote(category, file_id)
            img = cv2.imdecode(np.fromstring(file_bytes, np.uint8), cv2.IMREAD_COLOR)
            height, width = img.shape[:2]
            entry = file_dict.get(category)
            if entry is None:
                file_dict[category] = [{
                    "height": height,
                    "width": width,
                    "file": file,
                    "file_bytes": file_bytes
                }]
            else:
                file_dict[category].append({
                    "height": height,
                    "width": width,
                    "file": file,
                    "file_bytes": file_bytes
                })

        return file_dict

    def pick_watermark(self, category, img_height, alpha):
        watermarks = self.dict.get(category)
        if watermarks is None:
            self.dict = self.create_dict()
            watermarks = self.dict.get(category)
            if watermarks is None:
                return None, None, None
        scalings = list(map(lambda watermark: ((img_height / alpha) / watermark.get("height") - 1), watermarks))
        min_index = scalings.index(min(scalings, key=abs))
        scaling = (scalings[min_index] + 1) * 100
        scaling = 60 if scaling < 60 else scaling
        scaling = 140 if scaling > 140 else scaling
        return watermarks[min_index].get("file").split("/")[2], watermarks[min_index].get("file_bytes"), scaling
